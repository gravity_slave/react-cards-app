"use strict";var _express = require("express");var _express2 = _interopRequireDefault(_express);
var _http = require("http");var _http2 = _interopRequireDefault(_http);

var _settings = require("./settings");function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

// ----------------------
// Setup
var app = (0, _express2.default)();
var server = new _http2.default.Server(app);

// ----------------------
// Configuration
app.set("view engine", "pug");
app.use(_express2.default.static("public"));

var useExternalStyles = !_settings.isDevelopment;
var scriptRoot = _settings.isDevelopment ?
"http://localhost:8080/build" :
"/build";

app.get("*", function (req, res) {
	res.render("index", {
		useExternalStyles: useExternalStyles,
		scriptRoot: scriptRoot });

});

// ----------------------
// Startup
var port = process.env.PORT || 3000;
server.listen(port, function () {
	console.log("Started http server on " + port);
});
//# sourceMappingURL=server.js.map
